(define-module (development)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages version-control)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download))
(define-public partialclone
  (package
   (name "partialclone")
   (version "0.1.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://codeberg.org/tusharhero/partialclone")
           (commit version)))
     (file-name (git-file-name name version))
     (sha256 (base32 "1idkdnz7fzsksvzpkglxk93cx761hm8d43bbfq81m6wms8mhqqp9"))))
   (build-system copy-build-system)
   (propagated-inputs
    (list git))
   (arguments
    '(#:install-plan '(("partialclone.sh" "bin/partialclone"))))
   (home-page "https://codeberg.org/tusharhero/partialclone")
   (synopsis "Partial clone for Git.")
   (description "Clone a single directory or file from a Git repository.")
   (license license:gpl3+)))
