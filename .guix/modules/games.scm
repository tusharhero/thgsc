(define-module (games)
  #:use-module (guix)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages networking)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (gnu packages admin))
(define-public nettactoe
  (package
   (name "nettactoe")
   (version "1.3.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://codeberg.org/tusharhero/nettactoe")
           (commit version)))
     (file-name (git-file-name name version))
     (sha256 (base32 "0xnmgwp6fi7xzaj2fz4wlywx606vaaw0xxmg55k4d84syxha076n"))))
   (build-system copy-build-system)
   (propagated-inputs
    (list netcat))
   (arguments
    '(#:install-plan '(("nettactoe" "bin/"))))
   (home-page "https://codeberg.org/tusharhero/nettactoe")
   (synopsis "Multiplayer Tic Tac Toe game")
   (description "A multiplayer Tic Tac Toe game, written in GNU
   Bash. The multiplayer element is implemented with help of GNU
   Netcat.")
   (license license:gpl3+)))
(define-public nettactoe-relay
    (package
     (name "nettactoe-relay")
     (version "1.3.1")
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://codeberg.org/tusharhero/nettactoe")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256 (base32 "0xnmgwp6fi7xzaj2fz4wlywx606vaaw0xxmg55k4d84syxha076n"))))
     (build-system copy-build-system)
     (propagated-inputs
      (list socat))
     (arguments
      '(#:install-plan '(("nettactoe-relay" "bin/"))))
     (home-page "https://codeberg.org/tusharhero/nettactoe")
     (synopsis "Multiplayer Tic Tac Toe game relay server.")
     (description "Relay server for NetTacToe.")
     (license license:gpl3+)))
(define-public my-friend-tux
  (package
   (name "my-friend-tux")
   (version "7857886")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/gtlsgamr/my-friend-tux/")
           (commit version)))
     (file-name (git-file-name name version))
     (sha256 (base32 "1nlg5ghfahdlgkghyp9c9pgcsn0qkzpl4xyvasikgl7c12l0p1m7"))))
   (build-system copy-build-system)
   (arguments
    '(#:install-plan '(("mftux.sh" "bin/mftux"))))
   (home-page "https://github.com/gtlsgamr/my-friend-tux")
   (synopsis "A growing penguin that needs daily feeding")
   (description "This script is a digital companion for you. It can
   help you with absolutely nothing, other than just being
   cute. It's a simple script that displays a penguin which will
   grow as time goes by. You have to feed him by running the script
   every day. If you don't feed him, he will die. So, don't let him
   die. He also shows you the current date and time. That's
   it. Nothing more, nothing less. On the bright side, he is a good
   listener. You can talk to him whenever you feel lonely. He will
   listen to you without any judgement. So, go ahead and run the
   script. Have fun!" )
   (license license:gpl3)
   ))
